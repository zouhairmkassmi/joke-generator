import { NgModule } from '@angular/core';

import { JokeGeneratorSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [JokeGeneratorSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [JokeGeneratorSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class JokeGeneratorSharedCommonModule {}
